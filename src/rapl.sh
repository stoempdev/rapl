#!/bin/bash

function help {
	echo "  RAPL (Readable Apache PHP Logs) "
	echo "  rapl is a little script that displays Apache PHP error logs in a readable format"
	echo ""
	echo "  HELP"
	echo "    --help or -h      displays this help"
	echo ""
	echo "    -g use grc (needs to be installed)"
	echo "    -t use tail"
	echo "    -tf use tail -f"
	echo "    -tnXX use tail -nXX (nXX should be last argument)"
	echo "    -c use cat"
	echo "    -z use zcat"
	echo ""
	echo "  EXAMPLES"
	echo "    rapl -gt /path/to/log"
	echo "       == grc tail /path/to/log + layout regex"
	echo "    rapl -tf /path/to/log" 
	echo "       == tail -f /path/to/log  + layout regex"
	echo "    rapl -tn120 /path/to/log"
	echo "       == tail -n120 /path/to/log  + layout regex"
	echo "    rapl -c /path/to/log"
	echo "       == cat /path/to/log  + layout regex"
	echo ""
	echo "  VERSION"
	echo "    Version 0.1 -- 2017-03-06"
	echo ""
	echo "  COPYRIGHT"
	echo "    Copyright © 2016 Free Software Foundation, Inc.  License GPLv3+:"
	echo "    GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>."
    echo "    This  is free software: you are free to change and redistribute it.  "
    echo "    There is NO WARRANTY, to the extent permitted by law."
    echo ""
	echo "  AUTHOR"
	echo "    Quickly written by Dmitri GOOSENS (@dgoosens on twiiter, gitlab, github, irc, etc.)"
	echo ""
	echo ""
}



if [[ $1 = '-h' ]] || [[ $1 = '--help' ]]
then
	help
	exit
fi

if [[ $# < 2 ]]
then
	echo "  ERROR >> incorrect params"
	echo ""
	help
	exit
fi

options=$1
cmd=''
args=''
useGrc=false
re='^[0-9]+$'

if [[ "${options:0:1}" = "-" ]]
then
	for (( i=1; i<${#options} ; i++ ))
	do
		if [[ "${options:$i:1}" = "t" ]]
		then
			cmd='tail'
		elif [[ "${options:$i:1}" = "c" ]]
		then
			cmd='cat'
		elif [[ "${options:$i:1}" = "z" ]]
		then
			cmd='zcat'
		elif [[ "${options:$i:1}" = "f" ]]
		then
		    if [[ $args = '' ]]
            then
                args="-"
            fi
			args="$args""f"
		elif [[ "${options:$i:1}" = "n" ]]
		then
		    if [[ $args = '' ]]
            then
                args="-"
            fi
			args="$args""n"
        elif [[ "${options:$i:1}"  =~ $re ]]
        then
            if [[ $args = '' ]]
            then
                args="-n"
            fi
            args="$args""${options:$i:1}"
		elif [[ "${options:$i:1}" = "g" ]]
		then
			useGrc=true
		fi
	done
fi

if [[ $cmd = '' ]]
then
	echo "  ERROR >> please specify the parser t | l | c (see help)"
	echo ""
	help
	exit
fi

if [[ $useGrc = true ]]
then
    cmd="grc ""$cmd"
fi

path=""
count=0
for arg in "$@"
do
    if (( $count > 0 ))
    then
        path="$path"" $arg"
    fi
    count=$((count+1))
done

$cmd $args $path | sed -r 's/(.*Stack trace:.*)//g' | sed -r 's/(.*)(PHP)(\s*)([a-zA-Z ]*?:)(.*)/\n\n\n\2\3\4\n\5/g' | sed -r 's/(.*)(PHP)(.*)/\3/g'