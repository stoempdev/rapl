# Rapl -- Changelog

## 2017-03-06  >>  Version 0.1

- got fed up of auditing Apache error logs in the look for PHP errors|warnings|deprecated etc.
- creation of script for `tail -f` and added regex transformation
- added support for `cat` and `zcat`

