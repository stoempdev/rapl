# Rapl (Readable Apache PHP Logs) 

`rapl` is a little bash script that displays Apache PHP error logs in a readable format...  

It's just a very simple series of regex expressions that is applied to the log...  
This removes *unnecessary* stuff and allows the developer to focus on what matters to him.

***NB: In advance, sorry for the poor quality of the bash script... Am not an expert.***

Basically, it transforms this stdoutput (`tail`, `tail -f`, `cat`...):

```bash
[Mon Mar 06 14:02:26.612889 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP Notice:  Undefined index: cacheName in /var/www/project/libs/plugins/function.translate.php on line 47
[Mon Mar 06 14:02:26.612895 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP Stack trace:
[Mon Mar 06 14:02:26.612899 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   1. {main}() /var/www/project/web/index.php:0
[Mon Mar 06 14:02:26.612909 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   2. Application->run() /var/www/project/web/index.php:43
[Mon Mar 06 14:02:26.612914 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   3. Application->run() /var/www/project/sources/package/default/Application.php:110
[Mon Mar 06 14:02:26.612919 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   4. Smarty->fetch() /var/www/project/sources/package/default/Application.php:107
[Mon Mar 06 14:02:26.612924 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   5. Smarty_Internal_Template->getRenderedTemplate() /var/www/project/libs/Smarty.class.php:339
[Mon Mar 06 14:02:26.612929 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   6. Smarty_Internal_Template->renderTemplate() /var/www/project/libs/sysplugins/smarty_internal_template.php:569
[Mon Mar 06 14:02:26.612934 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   7. include() /var/www/project/libs/sysplugins/smarty_internal_template.php:434
[Mon Mar 06 14:02:26.612939 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   8. Smarty_Internal_Template->getRenderedTemplate() /var/www/project/sources/templates_c/ad2d7ba856dd9b334f16d534e67b2cb650c680a6.file.tpl_login.tpl.php:159
[Mon Mar 06 14:02:26.612945 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP   9. Smarty_Internal_Template->renderTemplate() /var/www/project/libs/sysplugins/smarty_internal_template.php:569
[Mon Mar 06 14:02:26.612949 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP  10. include() /var/www/project/libs/sysplugins/smarty_internal_template.php:434
[Mon Mar 06 14:02:26.612954 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP  11. smarty_function_translate() /var/www/project/sources/templates_c/8c48922e4c81e8ce180a7b6ee21ebd6dac7faffa.file.bottomLogin.tpl.php:30
[Mon Mar 06 14:02:26.612960 2017] [:error] [pid 12052] [client 127.0.0.1:55474] PHP  12. SmartyLibrary->translate() /var/www/project/libs/plugins/function.translate.php:59
```

into this:

```bash
 Notice:
  Undefined index: cacheName in /var/www/project/libs/plugins/function.translate.php on line 45

   1. {main}() /var/www/project/web/index.php:0
   2. Application->run() /var/www/project/web/index.php:43
   3. Application->run() /var/www/project/sources/package/default/Application.php:110
   4. Smarty->fetch() /var/www/project/sources/package/default/Application.php:107
   5. Smarty_Internal_Template->getRenderedTemplate() /var/www/project/libs/Smarty.class.php:339
   6. Smarty_Internal_Template->renderTemplate() /var/www/project/libs/sysplugins/smarty_internal_template.php:569
   7. include() /var/www/project/libs/sysplugins/smarty_internal_template.php:434
   8. Smarty_Internal_Template->getRenderedTemplate() /var/www/project/sources/templates_c/ad2d7ba856dd9b334f16d534e67b2cb650c680a6.file.tpl_login.tpl.php:159
   9. Smarty_Internal_Template->renderTemplate() /var/www/project/libs/sysplugins/smarty_internal_template.php:569
  10. include() /var/www/project/libs/sysplugins/smarty_internal_template.php:434
  11. smarty_function_translate() /var/www/project/sources/templates_c/8c48922e4c81e8ce180a7b6ee21ebd6dac7faffa.file.bottomLogin.tpl.php:30
  12. SmartyLibrary->translate() /var/www/project/libs/plugins/function.translate.php:59
```

## Usage

```bash
rapl -params /path/to/apache/log.log
```

**Params**

- `--help` or `-h`      displays  help
- `-g` use `grc` (needs to be installed)
- `-t` use `tail`
- `-tf` use `tail -f`
- `-tnXX` use `tail -nXX` *(nXX should be last argument)*
- `-c` use `cat`
- `-z` use `zcat`

**Examples**

```bash
rapl -gt /path/to/log       # > similar to grc tail /path/to/log (+ layout regex)
rapl -tf /path/to/log       # > similar to tail -f /path/to/log  (+ layout regex)
rapl -tn120 /path/to/log    # > similar to tail -n120 /path/to/log  (+ layout regex)
rapl -c /path/to/log        # > similar to cat /path/to/log  (+ layout regex)
```

## Install system wide

I put scripts like this in `/usr/local/sbin`.  
Feel free to put it elsewhere.

1. Either download the `rapl.sh` file directly or clone the repo
2. `cd` into the directory where the script is located (`/src`)
3. `chmod + x rapl.sh`
4. *On Debian:* `sudo mv rapl.sh /usr/local/sbin/rapl`

## Version

Version 0.1 -- 2017-03-06

## Copyright

Copyright © 2017 stoempDEV.com.  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.  
This  is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.